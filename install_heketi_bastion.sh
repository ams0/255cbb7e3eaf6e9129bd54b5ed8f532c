wget -qO- https://github.com/heketi/heketi/releases/download/v8.0.0/heketi-client-v8.0.0.linux.amd64.tar.gz|  tar xvz ;  chmod +x heketi-client/bin/heketi-cli ; sudo mv heketi-client/bin/heketi-cli /usr/local/bin
export HEKETI_CLI_SERVER="http://`oc get route -n glusterfs -o=jsonpath="{.items[*].status.ingress[*].host}"`"
export HEKETI_CLI_KEY=`oc get secret -n glusterfs heketi-storage-admin-secret -o template --template={{.data.key}}|base64 -d`
export HEKETI_CLI_USER=admin
heketi-cli cluster info